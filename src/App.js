import logo from "./logo.svg";
import "./App.css";
import Mainpage from "./component/Mainpage";
import Home from "./component/Home";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Video from "./component/Video";
import Auth from "./component/Auth";
import Account from "./component/Account";
import Reactrouter from "./component/Reactrouter";
import Posts from "./component/Posts";
import Welcome from "./component/Welcome";

import React, { Component } from "react";

export default class App extends Component {
  constructor() {
    super();
    this.changeAuth = this.changeAuth.bind(this);
    this.state = {
      isAuth: false,
    };
  }
  changeAuth = () => {
    this.setState({
      isAuth: true,
    });
  };
  render() {
    return (
      <div>
        <Router>
          <Mainpage />
          <Switch>
            <Route path="/" exact component={Reactrouter} />
            <Route path="/home" component={Home} />
            <Route path="/video" exact component={Video} />
            <Route path="/video/:id" exact component={Video} />
            <Route path="/video/:id/:category" exact component={Video} />
            <Route path="/posts/:id" component={Posts} />
            <Route
              path="/auth"
              render={(props) => (
                <Auth {...props} changeAuth={this.changeAuth} />
              )}
            />
            <Route path="/account" component={Account} />
            <Route
              path="/welcome"
              render={(props) => (
                <Welcome {...props} isAuth={this.state.isAuth} />
              )}
            />
          </Switch>
        </Router>
      </div>
    );
  }
}
