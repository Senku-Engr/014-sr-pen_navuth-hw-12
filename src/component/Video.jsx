import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function Video(props) {
  const opt = props.match.params.id;
  const category = props.match.params.category;
  const animation = (
    <div>
      <h1>Animation category</h1>
      <ul>
        <li>
          {" "}
          <Link as={Link} to="/video/animation/action">
            action
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/video/animation/romance">
            romance
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/video/animation/comedy">
            comedy
          </Link>
        </li>
      </ul>
    </div>
  );
  const movie = (
    <div>
      <h1>Movie category</h1>
      <ul>
        <li>
          {" "}
          <Link as={Link} to="/video/movie/adventure">
            adventure
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/video/movie/comedy">
            comedy
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/video/movie/crime">
            crime
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/video/movie/documentary">
            documentary
          </Link>
        </li>
      </ul>
    </div>
  );

  return (
    <div
      className="container-fluid"
      style={{ paddingLeft: "8%", paddingRight: "8%" }}
    >
      <ul>
        <li>
          {" "}
          <Link as={Link} to="/video/animation">
            animation
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/video/movie">
            movie
          </Link>
        </li>
      </ul>
      {opt === "animation" ? animation : null}
      {opt === "movie" ? movie : null}
      {category === undefined ? (
        <h3>please select a topic</h3>
      ) : (
        <h3>{category}</h3>
      )}
    </div>
  );
}

export default Video;
