import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import queryString from "query-string";

function Account(props) {
  let acc = queryString.parse(props.location.search);

  return (
    <div
      className="container-fluid"
      style={{ paddingLeft: "8%", paddingRight: "8%" }}
    >
      <h1>Account</h1>
      <ul>
        <li>
          {" "}
          <Link as={Link} to="/account?name=luffy">
            luffy
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/account?name=eren">
            eren
          </Link>
        </li>
        <li>
          {" "}
          <Link as={Link} to="/account?name=shinra">
            shinra
          </Link>
        </li>

        <li>
          {" "}
          <Link as={Link} to="/account?name=nezuko">
            nezuko
          </Link>
        </li>
      </ul>
      {acc.name === undefined ? (
        <h1>there is no name in the querystring</h1>
      ) : (
        <h1>
          the <span style={{ color: "red" }}>name</span> in the querystring is{" "}
          {`"${acc.name}"`}
        </h1>
      )}
    </div>
  );
}

export default Account;
