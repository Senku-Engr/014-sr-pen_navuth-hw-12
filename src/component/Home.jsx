import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import { Nav, Navbar, Form, FormControl, Button, Card } from "react-bootstrap";

function Home() {
  const arr = [1, 2, 3, 4, 5, 6];
  let data = arr.map((a) => (
    <div className="col-4">
      <Card>
        <Card.Img
          variant="top"
          src="https://images7.alphacoders.com/828/thumb-1920-828896.png"
        />
        <Card.Body>
          <Card.Title>Card title</Card.Title>
          <Card.Text>
            This card has supporting text below as a natural lead-in to
            additional content.{" "}
          </Card.Text>
          <Link to={`/posts/${a}`}> See more</Link>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>
    </div>
  ));
  return (
    <div
      className="container-fluid"
      style={{ paddingLeft: "8%", paddingRight: "8%" }}
    >
      <div className="row">{data}</div>
    </div>
  );
}

export default Home;
