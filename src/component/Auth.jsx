import React from "react";
import App from "../App";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import Welcome from "./Welcome";

function Auth(props) {
  return (
    <div
      className="container-fluid"
      style={{ paddingLeft: "8%", paddingRight: "8%" }}
    >
      <input type="text" placeholder="Username" />
      <input type="password" placeholder="Password" />

      <Link as={Link} to="/welcome">
        <button onClick={props.changeAuth}>Submit</button>
      </Link>
    </div>
  );
}

export default Auth;
