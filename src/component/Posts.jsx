import React from "react";

function Posts(props) {
  return (
    <div
      className="container-fluid"
      style={{ paddingLeft: "8%", paddingRight: "8%" }}
    >
      <h1>this content from post {props.match.params.id}</h1>
    </div>
  );
}

export default Posts;
